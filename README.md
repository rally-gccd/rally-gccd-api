FireStore structure:

```json
{
  teams: [
    {
      name:"Equipe test",
      colour:"#123456",
      login:"equipetest38",
      passwd:"T6578EIjfnzeçàgiçut",
      teammates: [
        {
          firstname: "Jordan",
          lastname: "Pauvel"
        }
      ],
      time: "1832767",
      sites: [
        {
          reference: {
            title: "Tour Effeil",
            location: "",
            qrCode: "kozf_u_u'éjrif",
            questionnaire: [
              {
                question: "C'est quoi ?",
                answers: ["truc", "Chouch"],
                goodAnswer: 1
              }
            ]
          },
          time: "1382842",
          goodAnswers: 3
        }
      ]
    }
  ],
  sites: [
    {
      title: "Tour Effeil",
      location: "",
      qrCode: "kozf_u_uéjrif",
      questionnaire: [
        {
          question: "C'est quoi ?",
          answers: ["truc", "Chouch"],
          goodAnswer: 1
        }
      ]
    }
  ]
}
```

![](https://gitlab.com/rally-gccd/rally-gccd-api/raw/master/ressources/Screenshot%20struct%20sites.png)

![](https://gitlab.com/rally-gccd/rally-gccd-api/raw/master/ressources/Screenshot%20struct%20sites.png)

Link to FireBase : https://console.firebase.google.com/project/rally-gccd/overview

Link to FireBase function :
* https://firebase.google.com/docs/functions/
* https://firebase.google.com/docs/functions/callable


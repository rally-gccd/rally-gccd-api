const functions = require('firebase-functions');
const admin = require('firebase-admin');
const bcrypt = require('bcrypt');

const SALT_ROUNDS = 10;

admin.initializeApp();
const settings = {timestampsInSnapshots: true};
const firestore = admin.firestore();
firestore.settings(settings);

exports.login = functions.https.onCall((data) => {
    let user, id;
    return firestore.collection("/teams").where("login", "==", data.login).get()
        .then( (snapshot) => {
            if(!snapshot.empty) {
                user = snapshot.docs[0].data().login;
                id = snapshot.docs[0].id;
                return firestore.collection("/passwords").doc(id).get().then((snapshot) => {
                    if(snapshot.exists) {
                        return bcrypt.compare(data.password, snapshot.data().password);
                    } else {
                        return {
                            success: false,
                            error: 2
                        };
                    }
                })
                .then((res) => {
                    if (!res.hasOwnProperty('error')) {
                        if(res) {
                            return bcrypt.hash(id + user, SALT_ROUNDS);
                        } else {
                            return {
                                success: false,
                                error: 2
                            }
                        }
                    } else {
                        return res;
                    }
                })
                .then(result => {
                    if(result.hasOwnProperty("success")) {
                        return result;
                    } else {
                        return {
                            success: true,
                            token: "\"" + result + "\"",
                            id: id
                        }
                    }
                });
            } else {
                return {
                    success: false,
                    error: 2
                };
            }
        });
});

// exports.createAdmin = functions.https.onCall(() => {
//     return bcrypt.hash("admin", SALT_ROUNDS, (err, hash) => {
//         if(!err) {
//             return firestore.collection("/admins").doc().set({login: "admin", password: hash});
//         } else {
//             return "prout";
//         }
//     });
// });

exports.adminLogin = functions.https.onCall((data, context) => {
    let user, id;
    if(context.rawRequest.headers.hasOwnProperty("x-appengine-user-ip")) {
        return firestore.collection("/admins").where("login", "==", data.login).get()
            .then( (snapshot) => {
                if(!snapshot.empty) {
                    user = snapshot.docs[0].data().login;
                    id = snapshot.docs[0].id;
                    return bcrypt.compare(data.password, snapshot.docs[0].data().password);
                } else {
                    return {
                        success: false,
                        error: 2
                    };
                }
            })
            .then((res) => {
                if (!res.hasOwnProperty('error')) {
                    if(res) {
                        return bcrypt.hash(context.rawRequest.headers["x-appengine-user-ip"] + id + user, SALT_ROUNDS);
                    } else {
                        return {
                            success: false,
                            error: 1
                        }
                    }
                } else {
                    return res;
                }
            })
            .then(result => {
                if(result.hasOwnProperty("success")) {
                    return result;
                } else {
                    return {
                        success: true,
                        token: result.toString()
                    }
                }
            });
    } else {
        return {
            success: false,
            error: 6
        }
    }

});

exports.createTeam = functions.https.onCall(data => {
    if(data.hasOwnProperty("login") && data.hasOwnProperty("password") && data.hasOwnProperty("teammates") && data.hasOwnProperty("colour")) {
        let pass;
        let doc;
        let id;
        return bcrypt.hash(data.password, SALT_ROUNDS).then(hash => {
            pass = hash;
            let result = {
                name: data.name,
                login: data.login,
                colour: data.colour,
                teammates: data.teammates
            };

            doc = firestore.collection("/teams").doc();
            id = doc.id;
            return doc.set(result);
        }).then(() => {
            if(id !== undefined) {
                return {
                    success: true,
                    data: firestore.collection("/passwords").doc(id).set({password: pass})
                }
            } else {
                return {
                    success: false
                }
            }
        });
    } else {
        return {
            success: false,
            error: 3
        };
    }
});

exports.startRally = functions.https.onCall(data => {
    if(data.hasOwnProperty("id")) {
        firestore.collection("/teams").doc(data.id).get().then(snapshot => {
            if(snapshot.exists) {
                return firestore.collection("/teams").doc(data.id).update({
                    startTime: admin.firestore.FieldValue.serverTimestamp()
                });
            } else {
                return {
                    success: false,
                    error: 4
                };
            }
        })
        .then(result => {
            if(!result.hasOwnProperty("error")) {
                return { success: true };
            } else {
                return result;
            }
        });
    }
});

exports.adminCheckToken = functions.https.onCall((data, context) => {
    if(data.hasOwnProperty("token") && data.hasOwnProperty("user") && context.rawRequest.headers.hasOwnProperty("x-appengine-user-ip")) {
        return firestore.collection("/admins").where("login", "==", data.user).get().then(snapshot => {
            if(!snapshot.empty) {
                return bcrypt.compare(context.rawRequest.headers["x-appengine-user-ip"] + snapshot.docs[0].id + snapshot.docs[0].data().login, data.token);
            } else {
                return {
                    valid: false
                };
            }
        })
            .then(res => {
                if(res.hasOwnProperty("valid")) {
                    return res;
                } else {
                    if(res) {
                        return {
                            valid: true
                        };
                    } else {
                        return {
                            valid: false
                        };
                    }
                }
            });
    } else {
        return {
            valid: false
        };
    }
});

exports.checkToken = functions.https.onCall((data, context) => {
    if(data.hasOwnProperty("token") && data.hasOwnProperty("user") && context.rawRequest.headers.hasOwnProperty("x-appengine-user-ip")) {
        return firestore.collection("/teams").where("login", "==", data.user).get().then(snapshot => {
            if(!snapshot.empty) {
                return bcrypt.compare(context.rawRequest.headers["x-appengine-user-ip"] + snapshot.docs[0].id + snapshot.docs[0].data().login, data.token);
            } else {
                return {
                    valid: false
                };
            }
        })
            .then(res => {
                if(res.hasOwnProperty("valid")) {
                    return res;
                } else {
                    if(res) {
                        return {
                            valid: true
                        };
                    } else {
                        return {
                            valid: false
                        };
                    }
                }
            });
    } else {
        return {
            valid: false
        };
    }
});

exports.getSiteQuestions = functions.https.onCall((data) => {
    if(data.hasOwnProperty("teamId") && data.hasOwnProperty("siteId")) {
        let questionnaire;
        return firestore.collection("/sites").doc(data.siteId).get().then((doc) => {
            if (doc.exists) {
                if(doc.data().isStart) {
                    return {
                        success: true,
                        payload: 4 //Start site
                    };
                } else if (doc.data().isFinish) {
                    return {
                        success: true,
                        payload: 5 //Finish site
                    };
                } else {
                    questionnaire = doc.data().questionnaire;
                    return firestore.collection("/teams").doc(data.teamId).get();
                }
            } else {
                return {
                    success: false,
                    payload: 1 //Site not found
                };
            }
        })
        .then((ret) => {
            if(ret.hasOwnProperty("exists") && ret.exists) {
                let totalQuestions = Object.keys(questionnaire).length;
                const startQuestionnaire = {
                    success: true,
                    payload: questionnaire,
                    questionCount: totalQuestions,
                    currentIndex: 0
                };

                if(ret.data().hasOwnProperty("sites")) {
                    if(ret.data().sites.hasOwnProperty(data.siteId) && ret.data().sites[data.siteId].hasOwnProperty("questionIndex")) {
                        //Team has already visited this site before
                        let currentIndex = ret.data().sites[data.siteId].questionIndex;
                        if(currentIndex < totalQuestions) {
                            //There are still questions unanswered
                            let remainingQuestions = {};
                            for(let i = currentIndex; i < totalQuestions; i++) {
                                remainingQuestions[i - currentIndex] = questionnaire[i];
                            }

                            return {
                                success: true,
                                payload: remainingQuestions,
                                questionCount: totalQuestions,
                                currentIndex: currentIndex
                            }
                        } else {
                            return {
                                success: false,
                                payload: 2 //Site already visited
                            };
                        }
                    } else {
                        return startQuestionnaire;
                    }
                } else {
                    return startQuestionnaire;
                }
            } else {
                if(ret.hasOwnProperty("payload")) {
                    return ret;
                } else {
                    return {
                        success: false,
                        payload: 6 //Player does not exist
                    };
                }
            }
        });
    } else {
        return {
            success: false,
            payload: 3 //Malformed request
        }
    }
});

function teamCheckToken(username, token) {
    if(username && token) {
        return firestore.collection("/teams").where("login", "==", data.user).get().then(snapshot => {
            if(!snapshot.empty) {
                return bcrypt.compare(context.rawRequest.headers["x-appengine-user-ip"] + snapshot.docs[0].id + snapshot.docs[0].data().login, data.token);
            } else {
                return {
                    valid: false
                };
            }
        })
            .then(res => {
                if(res.hasOwnProperty("valid")) {
                    return res;
                } else {
                    if(res) {
                        return {
                            valid: true
                        };
                    } else {
                        return {
                            valid: false
                        };
                    }
                }
            });
    } else {
        return {
            valid: false
        };
    }
}